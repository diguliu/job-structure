# Job Structure

A simple job structure that can be sorted into a job sequence where every job
appears after all of its dependencies.

## Solution

First of all, we need to parse the provided input into a more convenient
structure. A hash is a very intuitive struture for this since we can map
each job to its dependency and in the end we basically have multiple dependency
trees represented in that hash. For example, the following structure:

```
a =>
b => c
c => f
d => a
e => b
f =>
g => 

```

Would create the following trees:

```
  d  e
 /    \
a      b
        \
         c
          \
           f
```

To solve the actual problem we can use a recursive approach since we must
ensure every job requirement comes before the job itself. In our tree model,
this means that for every job (node) we can only include it to the
final sequence after we included its dependency (child). So we iterate through
the jobs' list and before adding it to the sequence we first try to include its
dependency and so on. When we get to a leaf job (without dependency), we can
then add it and backtrack the recursion adding jobs bottom-up. When we get to
the root job (job that was first chosen on the iteration) then we start the
process again with the next on the list.

Since we're checking in the begining of each inclusion if the job was already
added to the sequence, there is no problem in starting this process at an
intermediate job (like c on our example). When we start the process again with
a job closer to the root (like b) the recursion will stop on the closest job
already added to the sequence (c) instead of going all the way to the leaf
once again.

## Running the tests

To run the tests first install rspec:

```
bundle
```

Then:

```
bin/rspec
```

## Authors

* **Rodrigo Souto** - [Gitlab](https://gitlab.com/diguliu)

## License

This project is licensed under the MIT License - see the 
[LICENSE.md](LICENSE.md) file for details
