class JobStructure
  attr_accessor :input, :dependencies

  class SelfDependencyError < StandardError
    def message
      "Jobs can't depend on themselves!"
    end
  end

  class CircularDependencyError < StandardError
    def message
      "Jobs can't have circular dependency!"
    end
  end

  def initialize(input)
    @input = input
    @dependencies = parsed_dependencies
  end

  # Recursively looks for the job dependencies and backtracks the dependency
  # tree including jobs to the sequence from the leaf to the root.
  def include_job(job, visited)
    return if @sequence.match(job)

    raise SelfDependencyError if job == dependencies[job]

    unless dependencies[job].nil?
      raise CircularDependencyError if visited.include?(dependencies[job])

      visited << job
      include_job(dependencies[job], visited)
    end

    @sequence += job
  end

  # Iterates through jobs including them to the sequence after including each of
  # their dependencies.
  def sorted
    @sequence = ''
    dependencies.keys.each { |job| include_job(job, []) }
    return @sequence
  end

  # Convert input string into a structured dependencies hash
  def parsed_dependencies
    result = {}

    input.strip.split("\n").each do |dependency|
      job, requirement = dependency.split('=>').map(&:strip)
      result[job] = requirement
    end

    return result
  end
end
