require 'job_structure'

RSpec.describe JobStructure do
  let(:job_structure) { JobStructure.new(input) }

  describe '#parsed_dependencies' do
    subject { job_structure.parsed_dependencies }

    context 'empty input' do
      let(:input) { '' }
      it { is_expected.to eql({}) }
    end

    context 'one job without dependency' do
      let(:input) { 'a =>' }
      it {
        is_expected.to have_key('a')
        expect(subject['a']).to be_nil
      }
    end

    context 'multiple jobs without dependency' do
      let(:input) { %q(
        a =>
        b =>
        c =>
      ) }
      it {
        is_expected.to have_key('a')
        expect(subject['a']).to be_nil
        is_expected.to have_key('b')
        expect(subject['b']).to be_nil
        is_expected.to have_key('c')
        expect(subject['c']).to be_nil
      }
    end

    context 'multiple jobs with dependencies' do
      let(:input) { %q(
        a =>
        b => c
        c => a
      ) }
      it {
        is_expected.to have_key('a')
        expect(subject['a']).to be_nil
        is_expected.to have_key('b')
        expect(subject['b']).to eql('c')
        is_expected.to have_key('c')
        expect(subject['c']).to eql('a')
      }
    end
  end

  describe '#sorted' do
    subject { job_structure.sorted }

    def respect_order(requirement, job)
    end

    context 'empty input' do
      let(:input) { '' }
      it { is_expected.to eql('') }
    end

    context 'single without dependency' do
      let(:input) { 'a =>' }
      it { is_expected.to match('a') }
    end

    context 'multiple jobs without dependency' do
      let(:input) { %q(
        a =>
        b =>
        c =>
      ) }

      it { is_expected.to match('a') }
      it { is_expected.to match('b') }
      it { is_expected.to match('c') }
    end

    context 'multiple jobs with a single dependency ' do
      let(:input) { %q(
        a =>
        b => c
        c =>
      ) }

      it { is_expected.to match('a') }
      it { is_expected.to match('b') }
      it { is_expected.to match('c') }
      it { is_expected.to be_ordered('c', 'b') }
    end

    context 'multiple jobs with multiple dependencies' do
      let(:input) { %q(
        a =>
        b => c
        c => f
        d => a
        e => b
        f =>
      ) }

      it { is_expected.to match('a') }
      it { is_expected.to match('b') }
      it { is_expected.to match('c') }
      it { is_expected.to match('d') }
      it { is_expected.to match('e') }
      it { is_expected.to match('f') }
      it { is_expected.to be_ordered('c', 'b') }
      it { is_expected.to be_ordered('f', 'c') }
      it { is_expected.to be_ordered('a', 'd') }
      it { is_expected.to be_ordered('b', 'e') }
    end

    context 'job with a self-dependency ' do
      let(:input) { %q(
        a =>
        b =>
        c => c
      ) }

      it { expect{ subject }.to raise_error(JobStructure::SelfDependencyError) }
    end

    context 'structure with circular dependency' do
      let(:input) { %q(
        a =>
        b => c
        c => f
        d => a
        e =>
        f => b
      ) }

      it { expect{ subject }.to raise_error(JobStructure::CircularDependencyError) }
    end
  end
end
