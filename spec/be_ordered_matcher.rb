RSpec::Matchers.define :be_ordered do |requirement, job|
  match do |sequence|
    sequence.index(requirement) < sequence.index(job)
  end

  failure_message do |sequence|
    "expected that '#{sequence}' had '#{requirement}' before '#{job}'"
  end
  
  failure_message_when_negated do |actual|
    "expected that '#{sequence}' did not have '#{requirement}' before '#{job}'"
  end
end
